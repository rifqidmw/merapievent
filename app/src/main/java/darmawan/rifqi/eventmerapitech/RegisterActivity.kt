package darmawan.rifqi.eventmerapitech

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log

import android.view.MenuItem
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import darmawan.rifqi.eventmerapitech.adapter.GenreAdapter
import darmawan.rifqi.eventmerapitech.adapter.MemberAdapter
import darmawan.rifqi.eventmerapitech.model.Filter.response.ResponseFilter
import darmawan.rifqi.eventmerapitech.model.Filter.response.ResponseFilterResult
import darmawan.rifqi.eventmerapitech.model.Prepare.response.ResponsePrepare
import darmawan.rifqi.eventmerapitech.model.Prepare.response.ResponsePrepareDataGenre
import darmawan.rifqi.eventmerapitech.model.Prepare.value.PrepareModel
import darmawan.rifqi.eventmerapitech.model.Register.response.ResponseRegister
import darmawan.rifqi.eventmerapitech.model.Register.value.RegisterData
import darmawan.rifqi.eventmerapitech.model.Register.value.RegisterModel
import darmawan.rifqi.eventmerapitech.retrofit.BaseApiService
import darmawan.rifqi.eventmerapitech.retrofit.UtilsApi
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.properties.Delegates


class RegisterActivity : AppCompatActivity() {

    lateinit var mApiService: BaseApiService
    lateinit var mContext: Context

    lateinit var name: EditText
    lateinit var email: EditText
    lateinit var phone: EditText
    lateinit var kota: EditText
    lateinit var komunitas: EditText

    private var rvViewGenre: RecyclerView? =null
    private var adapterGenre by Delegates.notNull<GenreAdapter>()
    private var layoutManagerGenre: RecyclerView.LayoutManager? = null
    private var listGenre: ArrayList<ResponsePrepareDataGenre> = ArrayList()

    var sex = ""
    var genre = ""
    internal var sb: StringBuilder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        mApiService = UtilsApi.apiService

        name = etName
        email = etEmail
        phone = etPhone
        kota = etKota
        komunitas = etKomunitas

        rg_sex.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.rb_laki){
                sex = "1"
            } else if (checkedId == R.id.rb_perempuan){
                sex = "2"
            }
        }

//        rg_genre.setOnCheckedChangeListener { group, checkedId ->
//            if (checkedId == R.id.rb_strategy){
//                genre = "1"
//            } else if (checkedId == R.id.rb_mmorpg){
//                genre = "2"
//            } else if(checkedId == R.id.rb_casual){
//                genre = "3"
//            }
//        }

        rvViewGenre = rv_genre
        layoutManagerGenre = LinearLayoutManager(this@RegisterActivity) as RecyclerView.LayoutManager?
        listGenre = ArrayList()
        rvViewGenre!!.setHasFixedSize(true)
        rvViewGenre!!.layoutManager = layoutManagerGenre
        adapterGenre = GenreAdapter(this@RegisterActivity, listGenre)
        rvViewGenre!!.adapter = adapterGenre

        prepareData()
    }

    private fun prepareData(){
        val prepareModel = PrepareModel()
        prepareModel.HeaderCode = "0103"
        prepareModel.signature = "462a8ce9f5b6b2d838a03cf77669fafd137618f5"

        mApiService.prepare(prepareModel)
            .enqueue(object : Callback<ResponsePrepare> {
                override fun onResponse(call: Call<ResponsePrepare>, response: Response<ResponsePrepare>) {
                    if (response.isSuccessful){
                        if (response.body()!!.status == "200"){
                            listGenre = response.body()!!.data.genre
                            adapterGenre = GenreAdapter(this@RegisterActivity,listGenre)
                            adapterGenre!!.notifyDataSetChanged()
                            rvViewGenre!!.adapter = adapterGenre

                            val message = response.body()!!.message
                            Toast.makeText(this@RegisterActivity, message, Toast.LENGTH_SHORT).show()
                        } else {
                            val message = response.body()!!.message
                            Toast.makeText(this@RegisterActivity, message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Log.d("pg", "asdasd")
                        val message = response.body()!!.message
                        Toast.makeText(this@RegisterActivity, message, Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ResponsePrepare>, t: Throwable) {

                }
            })
    }

    override fun onCreateOptionsMenu(menu: android.view.Menu): Boolean {
        menuInflater.inflate(R.menu.register_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()

        if (id == R.id.action_add) {
            val registerModel = RegisterModel()
            registerModel.HeaderCode = "0104"
            registerModel.signature = "462a8ce9f5b6b2d838a03cf77669fafd137618f5"

//            sb = StringBuilder()
//
//            for (i in 0 until adapterGenre.checkedGenre.size){
//                val dataGenre = adapterGenre.checkedGenre[i]
//
//                sb!!.append(dataGenre.id)
//                genre = sb.toString()
//            }

            val registerData = RegisterData()
            registerData.name = name.text.toString()
            registerData.email = email.text.toString()
            registerData.phone = phone.text.toString()
            registerData.kota = kota.text.toString()
            registerData.komunitas = komunitas.text.toString()
            registerData.sex = sex
            registerData.id_genre = adapterGenre.selectedItem

            registerModel.data = registerData

            mApiService.register(registerModel)
                .enqueue(object : Callback<ResponseRegister> {
                    override fun onResponse(call: Call<ResponseRegister>, response: Response<ResponseRegister>) {
                        if (response.isSuccessful){
                            if (response.body()!!.status == "200"){
                                val message = response.body()!!.message
                                Toast.makeText(this@RegisterActivity, message, Toast.LENGTH_SHORT).show()
                            } else {
                                val message = response.body()!!.message
                                Toast.makeText(this@RegisterActivity, message, Toast.LENGTH_SHORT).show()
                            }
                        } else {
                            Log.d("pg", "asdasd")
                            val message = response.body()!!.message
                            Toast.makeText(this@RegisterActivity, message, Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onFailure(call: Call<ResponseRegister>, t: Throwable) {

                    }
                })
            startActivity(Intent(this@RegisterActivity, MainActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

}

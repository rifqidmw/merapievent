package darmawan.rifqi.eventmerapitech.model.Login.response

class ResponseLogin {
    lateinit var status: String
    lateinit var message: String
    lateinit var errorMessage: String
    lateinit var data: ResponseLoginData
}
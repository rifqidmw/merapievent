package darmawan.rifqi.eventmerapitech.model.Register.response

class ResponseRegisterData {
    lateinit var name: String
    lateinit var email: String
    lateinit var phone: String
    lateinit var kota: String
    lateinit var komunitas: String
    lateinit var sex: String
    lateinit var id_genre: String
    lateinit var update_at: String
    lateinit var created_at: String
    lateinit var id: String
}
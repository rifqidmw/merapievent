package darmawan.rifqi.eventmerapitech.model.Register.value

class RegisterData {
    lateinit var email: String
    lateinit var name: String
    lateinit var phone: String
    lateinit var komunitas: String
    lateinit var kota: String
    lateinit var sex: String
    lateinit var id_genre: String
}
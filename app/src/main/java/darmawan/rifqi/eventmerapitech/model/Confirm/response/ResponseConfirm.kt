package darmawan.rifqi.eventmerapitech.model.Confirm.response

class ResponseConfirm {
    lateinit var status: String
    lateinit var message: String
    lateinit var errorMessage: String
    lateinit var data: ResponseConfirmData
}
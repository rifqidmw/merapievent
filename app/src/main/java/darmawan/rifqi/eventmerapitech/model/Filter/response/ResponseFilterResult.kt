package darmawan.rifqi.eventmerapitech.model.Filter.response

class ResponseFilterResult {
    lateinit var id: String
    lateinit var name: String
    lateinit var email: String
    lateinit var phone: String
    lateinit var sex: String
    lateinit var genre_text: String
    lateinit var id_genre: String
    lateinit var komunitas: String
    lateinit var kota: String
    lateinit var status: String
    lateinit var status_text: String
}
package darmawan.rifqi.eventmerapitech.model.Filter.response

class ResponseFilter {
    lateinit var status: String
    lateinit var message: String
    lateinit var errorMessage: String
    lateinit var data: ResponseFilterData
}
package darmawan.rifqi.eventmerapitech.model.Confirm.response

class ResponseConfirmDetail {
    lateinit var id: String
    lateinit var name: String
    lateinit var email: String
    lateinit var phone: String
    lateinit var sex: String
    lateinit var genre: String
    lateinit var komunitas: String
    lateinit var kota: String
    lateinit var status: String
    lateinit var status_text: String
}
package darmawan.rifqi.eventmerapitech.model.Prepare.response

class ResponsePrepareData {
    lateinit var genre: ArrayList<ResponsePrepareDataGenre>
    lateinit var sexResponses: ArrayList<ResponsePrepareDataSex>
}
package darmawan.rifqi.eventmerapitech.model.Register.response

class ResponseRegister {
    lateinit var status: String
    lateinit var message: String
    lateinit var errorMessage: String
    lateinit var data: ResponseRegisterData
}
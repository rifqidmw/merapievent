package darmawan.rifqi.eventmerapitech.model.Prepare.response

class ResponsePrepare {
    lateinit var status: String
    lateinit var message: String
    lateinit var errorMessage: String
    lateinit var data: ResponsePrepareData
}
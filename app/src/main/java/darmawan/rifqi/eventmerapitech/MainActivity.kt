package darmawan.rifqi.eventmerapitech

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.View
import android.widget.Toast
import darmawan.rifqi.eventmerapitech.adapter.MemberAdapter
import darmawan.rifqi.eventmerapitech.model.Filter.response.ResponseFilter
import darmawan.rifqi.eventmerapitech.model.Filter.response.ResponseFilterResult
import darmawan.rifqi.eventmerapitech.model.Filter.value.FilterModel
import darmawan.rifqi.eventmerapitech.model.Filter.value.FilterData
import darmawan.rifqi.eventmerapitech.retrofit.BaseApiService
import darmawan.rifqi.eventmerapitech.retrofit.UtilsApi
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity(){
    lateinit var mApiService: BaseApiService
    lateinit var mContext: Context

    private var rvView: RecyclerView? =null
    private var adapter by Delegates.notNull<MemberAdapter>()
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var list: ArrayList<ResponseFilterResult> = ArrayList()

    private var isLoading by Delegates.notNull<Boolean>()
    private var page by Delegates.notNull<Int>()
    private var totalPage by Delegates.notNull<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fab.setOnClickListener {
            startActivity(Intent(this@MainActivity, RegisterActivity::class.java))
        }

        rvView = recyclerview
        layoutManager = LinearLayoutManager(this@MainActivity)
        list = ArrayList()
        rvView!!.setHasFixedSize(true)
        rvView!!.layoutManager = layoutManager
        adapter = MemberAdapter(this@MainActivity, list)
        rvView!!.adapter = adapter

        rvView!!.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })

        mApiService = UtilsApi.apiService

        page = 1
        totalPage = 0

        loadData()
        initListener()
    }

    private fun loadData(){
        val HeaderCode = "0101"
        val signature = "462a8ce9f5b6b2d838a03cf77669fafd137618f5"

        val member = FilterModel()
        member.HeaderCode = HeaderCode
        member.signature = signature

        val memberData = FilterData()
        memberData.email = ""
        memberData.name = ""
        memberData.phone = ""
        memberData.status = "*"
        memberData.page = page.toString()

        member.data = memberData

        mApiService.getData(member)
            .enqueue(object : Callback<ResponseFilter> {
                override fun onResponse(call: Call<ResponseFilter>, response: Response<ResponseFilter>) {
                    if (response.isSuccessful){
                        if (response.body()!!.status == "200"){
                            list = response.body()!!.data.result
                            if (page == 1){
                                adapter = MemberAdapter(this@MainActivity,list)
                                adapter!!.notifyDataSetChanged()
                                rvView!!.adapter = adapter
                            } else {
                                adapter.refreshAdapter(list)
                            }
                            totalPage = 1000
                        } else {
                            val message = response.body()!!.message
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Log.d("pg", "asdasd")
                        Toast.makeText(this@MainActivity, "Tidak dapat terhubung ke server", Toast.LENGTH_SHORT).show()
                    }
                    hideLoading()
                }

                override fun onFailure(call: Call<ResponseFilter>, t: Throwable) {

                }
            })
    }

    private fun initListener(){
        rvView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView?.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager?.itemCount
                val lastVisiblePosition = linearLayoutManager?.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition
                Log.d("count item: ", "countItem: $countItem")
                Log.d("last visible position: ", "lastVisiblePosition: $lastVisiblePosition")
                Log.d("last position: ", "isLastPosition: $isLastPosition")
                if (!isLoading && isLastPosition && page < totalPage) {
                    showLoading(true)
                    page = page.let { it.plus(1) }
                    loadData()
                }
            }
        })
    }

    private fun showLoading(isRefresh: Boolean) {
        isLoading = true
        progress_bar_horizontal_activity_main.visibility = View.VISIBLE
        rvView!!.visibility.let {
            if (isRefresh) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }

    private fun hideLoading() {
        isLoading = false
        progress_bar_horizontal_activity_main.visibility = View.GONE
        rvView!!.visibility = View.VISIBLE
    }

    override fun onCreateOptionsMenu(menu: android.view.Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        val item = menu.findItem(R.id.action_search)
        val searchView = item.actionView as? SearchView
        searchView!!.setQueryHint("Cari Nama...")
        searchView!!.setIconified(false)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                val HeaderCode = "0101"
                val signature = "462a8ce9f5b6b2d838a03cf77669fafd137618f5"

                val member = FilterModel()
                member.HeaderCode = HeaderCode
                member.signature = signature

                val memberData = FilterData()
                memberData.email = ""
                memberData.name = query
                memberData.phone = ""
                memberData.status = "*"
                memberData.page = "1"
                member.data = memberData
                mApiService.getData(member)
                    .enqueue(object : Callback<ResponseFilter> {
                        override fun onResponse(call: Call<ResponseFilter>, response: Response<ResponseFilter>) {
                            if (response.isSuccessful){
                                if (response.body()!!.status == "200"){
                                    list = response.body()!!.data.result
                                    adapter = MemberAdapter(this@MainActivity,list)
                                    adapter!!.notifyDataSetChanged()
                                    rvView!!.adapter = adapter
                                } else {
                                    val message = response.body()!!.message
                                    Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
                                }
                            } else {
                                Log.d("pg", "asdasd")
                                Toast.makeText(this@MainActivity, "Tidak dapat terhubung ke server", Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onFailure(call: Call<ResponseFilter>, t: Throwable) {

                        }
                    })
                return false
            }
        })

        return true
    }

}

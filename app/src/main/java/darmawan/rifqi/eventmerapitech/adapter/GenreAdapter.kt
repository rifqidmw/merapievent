package darmawan.rifqi.eventmerapitech.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import darmawan.rifqi.eventmerapitech.R
import darmawan.rifqi.eventmerapitech.model.Filter.response.ResponseFilterResult
import darmawan.rifqi.eventmerapitech.model.Prepare.response.ResponsePrepareData
import darmawan.rifqi.eventmerapitech.model.Prepare.response.ResponsePrepareDataGenre
import kotlinx.android.synthetic.main.item_data.view.*
import kotlinx.android.synthetic.main.item_genre.view.*
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import android.R.attr.label
import android.R.attr.label



class GenreAdapter(private val context: Context, private val arrayList: ArrayList<ResponsePrepareDataGenre>) : RecyclerView.Adapter<GenreAdapter.RecyclerViewHolder>() {
    var selectedPosition = -1

    val selectedItem: String
        get() {
            if (selectedPosition != -1) {
                return arrayList[selectedPosition].id
            }
            return ""
        }

    inner class RecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val radioButton: RadioButton

        init {
            radioButton = view.findViewById(R.id.rb_genre) as RadioButton
        }

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_genre, viewGroup, false)
        return RecyclerViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, i: Int) {
        var list = arrayList[i]
        holder.radioButton.isChecked = i == selectedPosition
        holder.radioButton.text = list.item
        holder.radioButton.setOnClickListener { v -> itemCheckChanged(v) }
    }
    private fun itemCheckChanged(v: View) {
        selectedPosition = v.tag as Int
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }
}
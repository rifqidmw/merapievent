package darmawan.rifqi.eventmerapitech.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import darmawan.rifqi.eventmerapitech.MainActivity
import darmawan.rifqi.eventmerapitech.R
import darmawan.rifqi.eventmerapitech.model.Confirm.response.ResponseConfirm
import darmawan.rifqi.eventmerapitech.model.Confirm.value.ConfirmData
import darmawan.rifqi.eventmerapitech.model.Confirm.value.ConfirmModel
import darmawan.rifqi.eventmerapitech.model.Filter.response.ResponseFilterResult
import darmawan.rifqi.eventmerapitech.model.Login.response.ResponseLogin
import darmawan.rifqi.eventmerapitech.retrofit.BaseApiService
import darmawan.rifqi.eventmerapitech.retrofit.UtilsApi
import kotlinx.android.synthetic.main.item_data.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.support.v4.content.ContextCompat.getSystemService
import kotlinx.android.synthetic.main.detail_user.view.*


class MemberAdapter (val context: Context, var list: ArrayList<ResponseFilterResult> = arrayListOf()) : RecyclerView.Adapter<MemberAdapter.MemberHolder>() {

    lateinit var mApiService: BaseApiService
    lateinit var dialogView: View
    lateinit var inflater: LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemberAdapter.MemberHolder {
        mApiService = UtilsApi.apiService
        return MemberHolder(LayoutInflater.from(context).inflate(R.layout.item_data, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MemberAdapter.MemberHolder, position: Int) {
        val list = list[position]

        holder.tvNama.text = list.name
        holder.tvEmail.text = list.email
        holder.tvKomunitas.text =list.komunitas

        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (list.status == "1"){
            holder.ivStatus.setBackgroundResource(R.drawable.yes)
        } else {
            holder.itemView.setOnClickListener {
                val builder = AlertDialog.Builder(context)
                dialogView = inflater.inflate(R.layout.detail_user, null)
                builder.setView(dialogView)
                builder.setTitle("Konfirmasi Peserta")
                builder.setMessage("Peserta atas nama ${list.name} menghadiri acara?")

                var nama = dialogView.tv_nama
                var email = dialogView.tv_email
                var phone = dialogView.tv_phone
                var sex = dialogView.tv_sex
                var kota = dialogView.tv_alamat
                var komunitas = dialogView.tv_komunitas
                var genre = dialogView.tv_genre

                nama.text = list.name
                email.text = list.email
                phone.text = list.phone
                sex.text = list.sex
                kota.text = list.kota
                komunitas.text = list.komunitas
                genre.text = list.genre_text

                builder.setPositiveButton("YA"){dialog, which ->
                    val confirmModel = ConfirmModel()
                    confirmModel.HeaderCode = "0102"
                    confirmModel.signature = "462a8ce9f5b6b2d838a03cf77669fafd137618f5"
                    val confirmData = ConfirmData()
                    confirmData.id = list.id
                    confirmModel.data = confirmData

                    mApiService.confirm(confirmModel)
                        .enqueue(object : Callback<ResponseConfirm> {
                            override fun onResponse(call: Call<ResponseConfirm>, response: Response<ResponseConfirm>) {
                                if (response.isSuccessful){
                                    if (response.body()!!.status == "200"){
                                        val message = response.body()!!.message
                                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                                    } else {
                                        val message = response.body()!!.message
                                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                                    }
                                } else {
                                    Log.d("pg", "asdasd")
                                    val message = response.body()!!.message
                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                                }
                            }

                            override fun onFailure(call: Call<ResponseConfirm>, t: Throwable) {

                            }
                        })
                }
                builder.setNegativeButton("Batal"){dialog, which ->

                }

                val dialog: AlertDialog = builder.create()
                dialog.show()

            }
        }

    }

    fun refreshAdapter(resultMember: List<ResponseFilterResult>) {
        this.list.addAll(resultMember)
        notifyItemRangeChanged(0, this.list.size)
    }

    inner class MemberHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvNama = itemView.tvNama
        var tvEmail = itemView.tvEmail
        var tvKomunitas = itemView.tvKomunitas
        var ivStatus = itemView.iv_status
    }
}
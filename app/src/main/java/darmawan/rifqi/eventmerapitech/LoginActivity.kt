package darmawan.rifqi.eventmerapitech

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import darmawan.rifqi.eventmerapitech.adapter.MemberAdapter
import darmawan.rifqi.eventmerapitech.model.Filter.response.ResponseFilter
import darmawan.rifqi.eventmerapitech.model.Login.response.ResponseLogin
import darmawan.rifqi.eventmerapitech.model.Login.value.LoginData
import darmawan.rifqi.eventmerapitech.model.Login.value.LoginModel
import darmawan.rifqi.eventmerapitech.retrofit.BaseApiService
import darmawan.rifqi.eventmerapitech.retrofit.UtilsApi
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.log

class LoginActivity : AppCompatActivity() {

    lateinit var mApiService: BaseApiService
    lateinit var mContext: Context

    lateinit var etEmail: EditText
    lateinit var etPassword: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mApiService = UtilsApi.apiService

        etEmail = et_email
        etPassword = et_password


        btn_login.setOnClickListener {
            if (etEmail.text.toString().isEmpty() || etEmail.text.toString() == ""){
                et_email.setError("Email harus diisi!")
            }
            if (etPassword.text.toString().isEmpty() || etPassword.text.toString() == ""){
                et_password.setError("Password harus diisi!")
            }

            val login = LoginModel()
            login.HeaderCode = "0105"
            login.signature = "462a8ce9f5b6b2d838a03cf77669fafd137618f5"

            val loginData = LoginData()
            loginData.email = etEmail.text.toString()
            loginData.password = etPassword.text.toString()

            login.data = loginData

            mApiService.login(login)
                .enqueue(object : Callback<ResponseLogin> {
                    override fun onResponse(call: Call<ResponseLogin>, response: Response<ResponseLogin>) {
                        if (response.isSuccessful){
                            if (response.body()!!.status == "200"){
                                val message = response.body()!!.message
                                Toast.makeText(this@LoginActivity, message, Toast.LENGTH_LONG).show()
                                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                            } else{
                                val message = response.body()!!.message
                                Toast.makeText(this@LoginActivity, message, Toast.LENGTH_LONG).show()
                            }
                        } else {
                            Log.d("pg", "asdasd")
                            val message = response.body()!!.message
                            Toast.makeText(applicationContext, "Email atau Password salah!!!", Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onFailure(call: Call<ResponseLogin>, t: Throwable) {

                    }
                })
        }
    }
}

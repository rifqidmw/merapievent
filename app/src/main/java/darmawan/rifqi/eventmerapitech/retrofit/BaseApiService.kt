package darmawan.rifqi.eventmerapitech.retrofit

import darmawan.rifqi.eventmerapitech.model.Confirm.response.ResponseConfirm
import darmawan.rifqi.eventmerapitech.model.Confirm.value.ConfirmModel
import darmawan.rifqi.eventmerapitech.model.Filter.value.FilterModel
import darmawan.rifqi.eventmerapitech.model.Filter.response.ResponseFilter
import darmawan.rifqi.eventmerapitech.model.Login.response.ResponseLogin
import darmawan.rifqi.eventmerapitech.model.Login.value.LoginModel
import darmawan.rifqi.eventmerapitech.model.Prepare.response.ResponsePrepare
import darmawan.rifqi.eventmerapitech.model.Prepare.value.PrepareModel
import darmawan.rifqi.eventmerapitech.model.Register.response.ResponseRegister
import darmawan.rifqi.eventmerapitech.model.Register.value.RegisterModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface BaseApiService {
    @POST("v1")
    fun getData(@Body filterModel: FilterModel): Call<ResponseFilter>

    @POST("v1")
    fun register(@Body registerModel: RegisterModel): Call<ResponseRegister>

    @POST("v1")
    fun login(@Body loginModel: LoginModel): Call<ResponseLogin>

    @POST("v1")
    fun prepare(@Body prepareModel: PrepareModel): Call<ResponsePrepare>

    @POST("v1")
    fun confirm(@Body confirmModel: ConfirmModel): Call<ResponseConfirm>
}
package darmawan.rifqi.eventmerapitech.retrofit

object UtilsApi {
    val BASE_URL_API = "http://event.merapitech.com/api/"

    val apiService: BaseApiService
        get() = RetrofitClient.getClient(BASE_URL_API).create(BaseApiService::class.java)
}